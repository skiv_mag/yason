#!/usr/bin/python

# system libraries
from os.path import exists, getmtime, join as path_join
import json
import yaml
import datetime
from email.utils import formatdate, parsedate
from time import mktime

# gevent
from gevent.pywsgi import WSGIServer

# settings module
import settings

# handler for data seriliazation(not supported by default with standart
# json serialization)


def dthandler(obj):
    if isinstance(obj, datetime.date) or isinstance(obj, datetime.datetime):
        return obj.isoformat()
    else:
        return None


def yaml_to_json(name):

    with open(name) as ya:
        yaml_file = yaml.load(ya.read())
        return json.dumps(yaml_file, default=dthandler, indent=4)


def json_response(env, start_response):

    file_path = env['PATH_INFO'].strip("/")
    yaml_file_path = path_join(settings.yaml_folder, file_path) + ".yaml"
    if file_path and exists(yaml_file_path):
        last_modified = getmtime(yaml_file_path)
        if 'HTTP_IF_MODIFIED_SINCE' in env and mktime(parsedate(env['HTTP_IF_MODIFIED_SINCE'])) < last_modified:
            start_response('304 Not Modified', [(
                'Content-Type', 'text/html;charset=utf-8')])
            return ['<h1>Not Modified</h1>']
        else:
            start_response('200 OK', [
                ('Content-Type', 'application/json;charset=utf-8'),
                ('Last-Modified', formatdate(
                 timeval=last_modified, localtime=False, usegmt=True))
            ]
            )
            return [yaml_to_json(yaml_file_path)]
    else:
        start_response('404 Not Found', [(
            'Content-Type', 'text/html;charset=utf-8')])
        return ['<h1>Not Found</h1>']


if __name__ == '__main__':
    print 'Serving yason_server on localhost:8888...'
    WSGIServer(('', 8888), json_response).serve_forever()
